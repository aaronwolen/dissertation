FILE=Wolen_Aaron_PhD

AUXDIR=aux

CHAPTERS := $(wildcard chapters/*.tex)
$(info $(CHAPTERS))

all: $(FILE).pdf

$(FILE).pdf: $(AUXDIR)/$(FILE).pdf
	cp $< $@

$(AUXDIR)/$(FILE).pdf: $(FILE).tex $(CHAPTERS)
	latexmk -pdf -output-directory=$(AUXDIR) $<

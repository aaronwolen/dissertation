Major corrections

Rita
[ X ] bottom of page 2 revise the botstein and gwas statement

[ ] page 4 need to add some nuance to my assertion that inbred strains are basically genetic clones. Perhaps cite Cook (2006) which describes a spontaneous mutation in BXD29 and Drake (1998) which provides an estimate of the mutation rate in mice specifically. 
f
[ X ] page 69 rewrite paragraph describing genomewide corrected pvalues. 

[ X ] SNP figure needs to be a table. 

[ X ] page 117 clarify "functionally relevant" SNPs

Minor corrections

[ ] URL fonts
[ ] spaces after probe-sets from probe command


Miles
[ X ] Figure 2.2 -- Should the array intensities follow a normal distribution? Why or why not?

[X] Figure 2.9 -- Explain the meaning of this. Why wouldn't there be some cross regional correlations? What if the relationships were non-linear or dependent on genotype? What about gsk3b?

(Maybe Arc, Lcn2 and Nr4a1 activate similar transcriptional responses in the PFC and NAc; you could check this by looking for overlap between Arc's top correlates in the PFC and NAc)

[ X ] Section 2.6.1 -- No hint of a biological discussion here. What could it mean to behaviors that some genes responded to ethanol in opposite directions across strains? Do any behaviors go in opposite directions?


[ ] Etanq1 Discussion -- This is it for the discussion? What about the other QTL regions? What about the relative lack of overlap between basal and ethanol anxiety QTL -- what does that mean?

[ X ] Chap 5 -- Which one (module)!?
[ X ] What about lack of correlation across modules despite membership overlaps? What does that imply?
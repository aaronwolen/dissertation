\begin{appendices}
\chapter{R code}

\section{\texttt{fishers\_sscore}}
\label{sec:fisher_sscore}
Function to identify ethanol responsive genes. 

{\singlespace\footnotesize\verbatiminput{r_code/fisher_sscore.R}}

\newpage	
\section{\texttt{ggAffy\_ProbePlot}}
\label{sec:ggaffy_probeplot}
Function to visualize probe-level intensity data for multiple probe-sets from an AffyBatch object. Colors can be mapped to samples using \verb|color.var| to specify a variable stored in the PhenoData slot.

{\singlespace\footnotesize\verbatiminput{r_code/ggAffy_ProbePlot.R}}

\newpage
\section{\texttt{ggAffy\_Hist}}
\label{sec:ggaffy_hist}
Simultaneously visualizing the probe intensity histograms for all samples in a dataset is a very useful quality assessment procedure. Outlier samples can be easily spotted with this approach, even when their deviation from the dataset isn't so apparent with boxplots of probe intensities. As with the default \verb|hist| function provided by the \verb|affy| package, you can specify whether histograms should be generated using intensities for PM probes, MM probes or both. However, intensity histograms generated for large datasets typically suffer from over-plotting, making it difficult to determine which line corresponds to which sample. \verb|ggAffy_Hist| provides a solution to this issue by allowing users to specify a subset of samples and comparing them to a reference array, which is added to the figure when \verb|median.ref = TRUE|. The reference array is created by calculating probe-wise medians across the entire dataset, not just the subset group. This makes it possible to see how the distributions of each subset compare to the larger dataset. I typically use this function in a loop that generates one figure for every 8 samples; any more and the readability begins to suffer. 

{\singlespace\footnotesize\verbatiminput{r_code/ggAffy_Hist.R}}

\newpage
\section{\texttt{snp\_prober}}
\label{sec:snp_prober}
As described in section \ref{sec:probe_snps}, unaccounted for polymorphisms within microarray probe target regions may affect probe/target hybridization and skew reported measurements of transcript abundance. This collection of functions was implemented to make convenient the process of identifying problematic probes on an Affymetrix GeneChip microarray. These functions rely heavily upon data provided by Bioconductor and depend on the following packages:

\begin{itemize}
	\item \verb|BSgenome|
	\item \verb|Biostrings|
	\item \verb|GenomicRanges|
	\item \verb|IRanges|
\end{itemize}

Optional arguments for \verb|find_probeSNPs| include \verb|probesets|, \verb|num.mm|, \verb|parallel| and \verb|n.cores|. The \verb|probesets| arguments makes it possible to specify a subset of probe-sets, by default all probe-sets that map to the specified chr are processed.  Setting the \verb|parallel| argument to \verb|TRUE| will enable the most resource-intensive components of this process to be distributed across multiple \gls{cpu} cores through integration with the \verb|foreach| package \citep{Analytics:2011fk}. Doing so substantially reduces the amount of time required for this analysis to complete. By default, all available cores are used. As using all available cores is not always desirable and in some cases may affect system stability, the user may specify the number of cores to make available with the \verb|n.cores| argument. The backend that \verb|foreach| relies on is provided by the \verb|multicore| package \citep{urbanek:2011lr}, which, as of this writing, only supports unix-like operating systems like Mac OS X, Linux and Solaris. Therefore \verb|snp_prober| cannot be run in parallel on Windows. 
	
The \verb|chr| argument is required because \verb|snp_prober| currently supports matching to one chromosome at a time. Although I plan to remove this limitation in future versions, the matching process is so resource-intensive that a user will typically want to split up probe-sets into smaller batches anyway and doing so by chr is a natural strategy. The \verb|probesets| argument allows the user to further subset the number of probes to process. Any probe-sets that do not map to the specified chr will be discarded. 
 
{\singlespace\footnotesize\verbatiminput{r_code/snp_prober/snp_prober.R}} 

\newpage
\subsection{\texttt{load\_annotations}}
Requires only the name of microarray platform to load all necessary annotation packages. 
{\singlespace\footnotesize\verbatiminput{r_code/snp_prober/load_annotations.R}} 


\newpage
\subsection{\texttt{match\_probe\_seqs}}
The primary pattern matching function of \verb|snp_prober|. Probe sequences are matched against the reference genome provided by \verb|BSgenome| and a data frame containing the coordinates of all matches within a \ac{chr} sequence are  returned. Prior to the matching operations, the strand from which the probe-set target is transcribed is determined by comparing the number of successful matches on the positive-strand versus successes on the negative strand. The \verb|num.mm| argument specified in \verb|snp_prober| is passed to \verb|match_probe_seqs|, and allows the user to indicate how many probe/reference mismatches are allowable. It should be noted that every additional allowable mismatch greatly increases the processing time required to identify matches. I generally set \verb|num.mm| to 1. 

{\singlespace\footnotesize\verbatiminput{r_code/snp_prober/match_probe_seqs.R}} 

\newpage
\subsection{\texttt{get\_ensembl\_exons}}
Although the vast majority of probes target sequences within exons, a subset of probes target regions harboring exon-exon junctions. Because \verb|match_probe_seqs| looks for probe sequence matches across genomic \gls{dna}, introns disrupt the alignment of exon spanning probes. This issue is handled by downloading the exon sequences for a probe target using \verb|get_ensembl_exons| and repeating search across the assembled transcript sequence with \verb|search_spanning_exons|.
{\singlespace\footnotesize\verbatiminput{r_code/snp_prober/get_ensembl_exons.R}} 
	
\subsection{\texttt{search\_spanning\_exons}}
{\singlespace\footnotesize\verbatiminput{r_code/snp_prober/search_spanning_exons.R}} 

\newpage
\subsection{\texttt{consolidate\_exons}}
The exon sequence data obtained from Ensembl is often massively redundant due to the existence of multiple isoforms or splice variants. \verb|consolidate_exons| does what it says and returns a single, representative transcript that encompasses all other exons. \autoref{fig:consolidated_exons} provides a visualization of a consolidated group of exons. 
{\singlespace\footnotesize\verbatiminput{r_code/snp_prober/consolidate_exons.R}}

\begin{figure}[tbp]
	\centering
		\includegraphics{consolidated_exons_ENSMUSG00000061232}
	\caption[A typical result produced by the \texttt{consolidate\_exons} function]{\textbf{A typical result produced by the \texttt{\textbf{consolidate\_exons}} function.} Yellow blocks represent the original exon sequences obtained from Ensembl, while purple blocks represent the consolidated product.}
	\label{fig:consolidated_exons}
\end{figure}


% \subsection{Challenges matching probe sequences}
% Many of the challenges involved in matching probe sequences to a genome are relatively trivial when doing so by hand. The difficulties arise in overcoming these challenges programmatically and consistently, so that the analysis can be scaled to simultaneously handle tens of thousands of probes. 
% 
% \subsubsection{Strandedness}
% A prerequisite for successfully matching probe sequences to a genome is knowing which strand is targeted by each probe. Unfortunately, the Bioconductor microarray probe data packages do not provide this information and indicate only that all probes are `antisense,' which is unhelpful. 
% 
% To avoid requiring the user to supply each probe-set target's strand, \verb|snp_prober| attempts to figure this out. Initially, \verb|match_probe_seqs| looks for sequences matches along the positive strand. If too few are found the negative strand is checked by transforming probe sequences to their reverse complement. The correct strand is then taken as the one which returned the most successful matches. 
% 
% It should be noted that the \verb|probe.start| and \verb|probe.end| columns returned by \verb|match_probe_seqs| indicate the coordinates of the matching sequence along the reference genome's positive strand, regardless of the strand from which a probe target is transcribed. This means \verb|probe.end| values are always larger than \verb|probe.start| values. The returned \verb|sequence|, however, is strand specific and matches the $5^\prime$--$3^\prime$ sequence of the target transcript.  

% \subsubsection{Consolidating exons}

\chapter{Supplemental tables}

All supplemental data can be downloaded at \href{http://aaronwolen.com/thesis}{aaronwolen.com/thesis}, including the following supplementary tables. 

\section{Table S1}
\label{sec:supptable_bxd_etoh_genes}
Lists of genes found to be significantly ethanol responsive in \ac{pfc}, \ac{nac} and \ac{vmb} by the analysis described in the \nameref{sec:results_etoh_responsive_genes} section. 

\section{Table S2}
\label{sec:supptable_bxd_etoh_genes_topp_results}
Full results from functional over-representation analysis of ethanol responsive genes in \ac{pfc}, \ac{nac} and \ac{vmb}, discussed in section \ref{sec:topp_analysis_etoh_genes}. 

\section{Table S3}
\label{sec:supptable_lxs_etoh_genes}
Lists of genes found to be significantly ethanol responsive in \ac{pfc} across the \ac{lxs} panel as part of the analysis in section  \ref{sec:results_etoh_responsive_genes_overlap}.

\section{Table S4}
\label{sec:supptable_bxd_pfc_prclqs}
List of genes that belong to the paraclique networks defined in the \nameref{sec:bxd_pfc_prclqs} section using saline versus ethanol \acp{sscore}, as well as the saline and ethanol \ac{rma} data for the \ac{pfc} data-set. Degree of connectivity and betweenness centrality measures are provided for each probe-set. 

\section{Table S5}
\label{sec:supptable_cross_treatment_prcla_overlap}
Overlap of paraclique networks constructed using \ac{sscore} data and the saline/ethanol \ac{rma} data-sets, as described in section \ref{sec:bxd_pfc_prclqs}.

\section{Table S6}
\label{sec:supptable_ergen_eqtls}
Peak \ac{eqtl} results for all members of the saline \ac{rma} and \ac{sscore} paracliques (section \ref{sec:bxd_pfc_prclqs}) with at least one suggestive \ac{eqtl} found in the analysis discussed in section \ref{sec:eqtl_results}.

\section{Table S7}
\label{sec:supptable_ergen_transband_candidates}
Ranking results of positional candidate genes within each of the major \ac{ergen} \acp{transband}.

\section{Table S8}
\label{sec:supptable_prclq_topp_results}
Full results from functional over-representation analysis of \ac{sscore} networks identified in the \ac{pfc}. 

\section{Table S9}
\label{sec:supp_b6d2_probe_snps}
List of Affymetrix \ac{m430} probe-sets that overlap one or more \ac{b6}\ac{d2} polymorphisms. 
\end{appendices}
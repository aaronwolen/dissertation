# fisher_sscore
################################################################################

# Identify Affymetrix probe-sets exhibiting significant expression variation

# ARGUMENTS
# sscores: matrix of sscore expression data generated with the SScore package

# parallel: logical, If TRUE multicore package is loaded and all analysis
# is split among all available cores.

# n.core: number of cores to use for parallel execution. If unspecified all
# available cores will be used. 

# frugal: logical, enables frugal mode, which dramatically reduces the
# amount of time required for calculating empirical p-values for a slight
# cost in accuracy. 

fisher_sscore <- function(sscores, n.perm, plot.results, 
  parallel = FALSE, n.cores, verbose = FALSE, frugal = FALSE){
  
  if(is.data.frame(sscores)) {
    sscores  <- as.matrix(sscores)
  }
  
  # Print status
  print_status <- function(message) {
    if(verbose) {
      dt <- format(Sys.time(), "%D %r")
      writeLines(paste("\n", dt, "-", message, "\n"))
    }
  }
  
  # Fisher's method to combine p-values
  fishers_method <- function(data){
    S <- apply(data, M = 1, F = function(x) sum(-2 * log(x)))
    pvalue <- pchisq(S, df = ncol(data) * 2, lower.tail = F)
    return(data.frame(S, pvalue, row.names = rownames(data)))
  }
  
  # Probeset resampling function
  permute <- function(x) {
    p <- sample(x, length(x), replace = F)
    return(p)
  }
  
  # Calculate empirical p-values from a matrix of permuted data
  calc_emp <- function(obs, perm) {
    emp.p <- apply(perm, 2, function(x) 
      sum(x > obs) / length(x))
    emp.p <- mean(emp.p)
    return(emp.p)
  }
  	
  # Identify number of available cores and register parallel backend
  if(parallel){
    print_status("Initializing multicore backend")
    require(multicore, quietly = T)
    require(doMC, quietly = T)
    available.cores <- multicore:::detectCores()
    
    if(available.cores < 2){
      parallel <- FALSE
      writeLines(paste("Sorry, you don't actually have a multicore CPU.\n",
                       "Perhaps it's time to upgrade?\n\n", sep = ""))		
    }
    
    if(missing(n.cores)){
      n.cores <- available.cores
    } else {
      if(n.cores > available.cores){
        n.cores <- available.cores
      }
    }
    registerDoMC(cores = n.cores)
  }

	# Convert sscores to pvalues
  print_status("Performing p-value transformation for observed data")
	pscores <- 2 * pnorm(abs(sscores), lower.tail = F)
	
  # Combine p-values
  print_status("Combining observed p-values")
	obs.results <- fishers_method(pscores)
	
	# Permutations
	##############
  print_status(paste("Performing", n.perm, "permutations"))

	# Generate permuted matrix and calculate significance values
	# Return a list containing an entry for each permutation
  if(parallel) {
    perm.results <- foreach(p = 1:n.perm) %dopar% {
      perm.data <- apply(pscores, M = 2, F = permute)
      fishers_method(perm.data)
    }
  } else {
    perm.results <- list()
    for(p in 1:n.perm) {
      perm.data <- apply(pscores, M = 2, F = permute)
      perm.results[[p]] <- fishers_method(perm.data)
    }
  }

	# Calculate empirical pvalues
  #############################
  print_status("Calculating empirical p-values")
  perm.s <- do.call("cbind", lapply(perm.results, function(x) x$S))
  
  # Obtain pvalues for entire range of observed S-values, rather than 
  # repeating calculation for every individual S-values
  if(frugal) {
    obs.range <- c(floor(min(obs.results$S)), ceiling(max(obs.results$S)))
    obs.S <- seq(obs.range[1], obs.range[2], .5)
  } else {
    obs.S <- obs.results$S
  }
  
  
  if(parallel) {
    epval <- mclapply(obs.S, function(x) calc_emp(x, perm.s))
    epval <- unlist(epval)
  } else {
    epval <- sapply(obs.S, function(x) calc_emp(x, perm.s))
  }
  
  # Align frugal pvalues with observed data
  if(frugal) {
    frug.results <- data.frame(S = obs.S, emp.pvalue = epval)
    frug.i <- cut(obs.results$S, breaks = obs.S, labels = F)
    obs.results$emp.pvalue <- frug.results$emp.pvalue[frug.i]    
  } else {
    obs.results$emp.pvalue <- epval
  }
  
	# Calculate observed and empirical qvalues
	obs.results$qvalue <- p.adjust(obs.results$pvalue, method = "fdr")
  obs.results$emp.qvalue <- p.adjust(obs.results$emp.pvalue, method = "fdr")
  
  print_status("Finished")
  return(obs.results)
}

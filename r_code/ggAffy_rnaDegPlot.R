# ggAffy_rnaDegPlot
################################################################################
# object: an AffyBatch-class
# which: character, use pm probes, mm probes or both
# samples: optional character vector of samples indicating a subset of arrays
# in AffyBatch to plot
# log2: logical, should intensity be log transformed
# median.ref: logical, generate and plot a common psuedo-array reference

# DETAILS
# A vector of samples can be supplied to generate histograms
# for a subset of the dataset. Setting median.ref to TRUE will facilitate 
# comparison of each of subset with the entire dataset by generating a 
# synthetic array created by calculating probe-wise medians and plotting
# its intensity histogram.

# EXAMPLE
# data(Dilution)
# ggAffy_Hist(Dilution, which = "pm")
# ggAffy_Hist(Dilution, which = "both")

# sample.sub <- sampleNames(Dilution)[1:2]
# ggAffy_Hist(Dilution, which = "pm", samples = sample.sub, median.ref = T)

library(affydata); data(Dilution)

ggAffy_rnaDegPlot <- function(object, samples, log2 = T, 
  scale = T, shift = F, median.ref = F){
  
  require(affy, quietly = T)
  require(ggplot2, quietly = T)
  
  if(class(object) != "AffyBatch"){
    stop("object must be an AffyBatch object.", call. = F)
  }
  
  # Use all samples if no subset is provided
  if(missing(samples)){
    samples <- sampleNames(object)
  }
  
  # Extract raw probe intensities
  exp <- pm(object[,samples], LIST = TRUE)
  
  
  if(median.ref){
    exp <- lapply(exp, function(x) cbind(x, median = rowMedians(x)))
  }
  
  if(log2){
    exp <- lapply(exp, log2)
  }
  
  # Use all samples if no subset is provided
  if(missing(samples)){
    samples <- sampleNames(object)
  }
  
  # Exclude probe-sets with non-standard number of probes
  prbs.n <- sapply(exp, nrow)
  prbs <- as.numeric(names(sort(table(prbs.n), dec = T))[1])
  exp <- exp[prbs.n == prbs]
  
  
  # Pre-populate matrix for probe position mean and sd
  sets.n <- length(exp)
  n <- ncol(exp[[1]])
  mns <- matrix(nrow = n, ncol = prbs)
  sds <- mns

  # Calculate each probe position's mean intensity
  for (p in 1:prbs) {
    exp.prb <- do.call("rbind", lapply(exp, function(x) x[i,]))
    if(n == 1){ 
      exp.prb <- t(exp.prb)
    }
    mns[, i] <- colMeans(exp.prb)
    sds[, i] <- apply(exp.prb, M = 2, sd)
  }
  
  # Calculate RNA degradation
  deg <- AffyRNAdeg(object[,samples], log.it = log2)
  
  # Keep only means and ses
  deg_df <- deg[c("means.by.number", "ses")]
  names(deg_df)[1] <- "means"
  
  # Add sample column and convert to long format data frame
  deg_df <- lapply(deg_df, function(x) 
    as.data.frame.table(matrix(x, ncol = ncol(x), dimnames = list(samples))))
  
  deg_df <- with(deg_df, merge(means, ses, by = c("Var1", "Var2")))
  names(deg_df) <- c("sample", "probe", "means", "ses")

  # Maintain original sample order
  deg_df$sample <- factor(deg_df$sample, levels = samples)
  
  # Set probes as ordered factor with numeric labels
  deg_df$probe <- factor(deg_df$probe, levels = levels(deg_df$probe),
    labels = 1:nlevels(deg_df$probe))
  
 
  
  # Scale
  if(scale){
    deg_df <- ddply(deg_df, .(sample), transform,
      means = (means - median(means)) / ses)
  } 

  # Shift
  if(shift){
    deg_df <- ddply(deg_df, .(sample), transform,
      means = means + (-1 * means[probe == 1]) + as.numeric(sample)[1])
  }
  
   print(deg_df)
  
  # Render plot
  ggplot(deg_df, aes(x = probe, group = sample)) + 
     geom_line(aes(y = means, color = sample))
}


ggAffy_rnaDegPlot(Dilution, log2 = T, scale = T, shift = T, median.ref = F)
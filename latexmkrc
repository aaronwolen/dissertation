# As suggested by latexmk author, John Collins: 
# http://www.techques.com/question/31-60204

# abbreviations
add_cus_dep('acn', 'acr', 0, 'run_makeindex');

# genes
add_cus_dep('gls', 'glo', 0, 'run_makeindex');

sub run_makeindex {
   my $source = $$Psource;
   my $dest = $$Pdest;
   my $log = $dest."LOG";
   my $cmd = "makeindex %O -s \"$_[0].ist\"  -t \"$log\" -o \"$dest\" \"$source\"";
   if ($silent) { $cmd =~ s/%O/-q/; }
   else { $cmd =~ s/%O//; }
   return system $cmd;
}
